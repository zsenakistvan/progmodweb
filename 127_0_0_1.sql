-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Gép: 127.0.0.1
-- Létrehozás ideje: 2020. Dec 01. 12:14
-- Kiszolgáló verziója: 8.0.18
-- PHP verzió: 7.3.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Adatbázis: `exceptionmanager`
--
CREATE DATABASE IF NOT EXISTS `exceptionmanager` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `exceptionmanager`;

DELIMITER $$
--
-- Eljárások
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `create_exception` (IN `message_in` VARCHAR(1000) CHARSET utf8, IN `stacktrace_in` TEXT CHARSET utf8, IN `type_in` VARCHAR(1000) CHARSET utf8, IN `source_in` VARCHAR(1000) CHARSET utf8)  NO SQL
INSERT INTO `exceptionmanager` (`exceptionmanager`.`date`, `exceptionmanager`.`message`, `exceptionmanager`.`stacktrace`, `exceptionmanager`.`source`, `exceptionmanager`.`type`, `exceptionmanager`.`guid`) VALUE (NOW(), message_in, stacktrace_in, source_in, type_in, UUID())$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delete_fiz_exception` (IN `id_in` INT(8))  NO SQL
DELETE FROM `exceptionmanager` WHERE `exceptionmanager`.`id` = id_in$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_all_exception` ()  NO SQL
SELECT * FROM `exceptionmanager` WHERE `exceptionmanager`.`is_active` = 1 ORDER BY `exceptionmanager`.`date` DESC$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `get_exception_by_guid` (IN `guid_in` VARCHAR(100) CHARSET utf8, OUT `id_out` INT(8))  NO SQL
SELECT `exceptionmanager`.`id` INTO id_out FROM `exceptionmanager` WHERE `exceptionmanager`.`guid` = guid_in AND `exceptionmanager`.`is_active` = 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `read_exception` (IN `id_in` INT(8))  NO SQL
SELECT * FROM `exceptionmanager` WHERE `exceptionmanager`.`id` = id_in AND `exceptionmanager`.`is_active` = 1$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `search_exception_by_message` (IN `message_in` VARCHAR(1000) CHARSET utf8)  NO SQL
SELECT * FROM `exceptionmanager` WHERE `exceptionmanager`.`message` LIKE message_in AND `exceptionmanager`.`is_active` = 1 ORDER BY `exceptionmanager`.`date` DESC$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `set_exception_to_active` (IN `id_in` INT(8))  NO SQL
UPDATE `exceptionmanager` SET `exceptionmanager`.`is_active` = 1 WHERE `exceptionmanager`.`id` = id_in$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `set_exception_to_inactive` (IN `id_in` INT(8))  NO SQL
UPDATE `exceptionmanager` SET `exceptionmanager`.`is_active` = 0 WHERE `exceptionmanager`.`id` = id_in$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `update_exception` (IN `source_in` VARCHAR(1000) CHARSET utf8, IN `id_in` INT(8))  NO SQL
UPDATE `exceptionmanager` SET `exceptionmanager`.`source` = source_in WHERE `exceptionmanager`.`id` = id_in$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Tábla szerkezet ehhez a táblához `exceptionmanager`
--

CREATE TABLE `exceptionmanager` (
  `id` int(8) NOT NULL,
  `date` datetime NOT NULL,
  `message` varchar(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `stacktrace` text COLLATE utf8_unicode_ci NOT NULL,
  `source` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `guid` varchar(100) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `is_active` int(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- A tábla adatainak kiíratása `exceptionmanager`
--

INSERT INTO `exceptionmanager` (`id`, `date`, `message`, `stacktrace`, `source`, `type`, `guid`, `is_active`) VALUES
(1, '2020-11-17 08:30:35', 'DB próba msg', 'DB próba st', 'DB próba source', 'DB próba type', '203e0fda-28ca-11eb-8290-9b6b492af860', 1),
(2, '2020-11-16 06:45:16', 'alam', 'körte', 'szilva', 'szőlő', '203e19f8-28ca-11eb-8290-9b6b492af860', 0),
(4, '2020-11-17 13:22:51', 'skjdhlfk', 'bjhvjkhvhkjvkjvkjgvkj kkg kjg kj i kj kj gkj kj ', 'hkhkghkhikh', 'ziuhkk', '9cd91846-28cf-11eb-8290-9b6b492af860', 1);

--
-- Indexek a kiírt táblákhoz
--

--
-- A tábla indexei `exceptionmanager`
--
ALTER TABLE `exceptionmanager`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `uniqueguid` (`guid`);

--
-- A kiírt táblák AUTO_INCREMENT értéke
--

--
-- AUTO_INCREMENT a táblához `exceptionmanager`
--
ALTER TABLE `exceptionmanager`
  MODIFY `id` int(8) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
