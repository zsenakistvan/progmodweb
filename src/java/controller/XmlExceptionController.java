/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import model.Kamu;
import model.SaveModelData;
import model.XmlException;
import service.XmlExceptionService;

/**
 *
 * @author zsenakistvan
 */
public class XmlExceptionController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
         try{
             PrintWriter out = response.getWriter();
             XmlExceptionService service = new XmlExceptionService();
             
             if(request.getParameter("task").equals("loadExceptions")){
                List<XmlException> exceptions = service.xmlExceptionLogs();
                out.write(service.toJson(exceptions).toString());
             }
             
             if(request.getParameter("task").equals("getExceptionData") && 
                     request.getParameter("guid") != null){
                 SaveModelData<Kamu> smd = new SaveModelData<Kamu>();
                 Kamu k = new Kamu(23);
                 smd.save(k, "KamuSzam");
                 SaveModelData<XmlExceptionService> smd2 = new SaveModelData<XmlExceptionService>();
                 XmlExceptionService es = new XmlExceptionService();
                 es.counter = 5;
                 smd2.save(es, "counter");
                 
                 
                XmlException ex = service.getExData(request.getParameter("guid"));
                out.write(service.exDataToJson(ex).toString());
             }
        }
         catch(Exception ex){
             System.out.println(ex.toString());
         }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
