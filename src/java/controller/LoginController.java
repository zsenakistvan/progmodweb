/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONObject;

/**
 *
 * @author zsenakistvan
 */
public class LoginController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("application/json");
        
        try (PrintWriter out = response.getWriter()) {
            if(request.getParameter("task") != null && 
                request.getParameter("task").equals("login") && 
                request.getParameter("username") != null){
                    request.getSession().setAttribute("username", request.getParameter("username"));
                    response.setStatus(200);
                    JSONObject jso = new JSONObject();
                    jso.put("result", "ok");
                    out.write(jso.toString());
            }
            
            if(request.getParameter("task") != null && 
                request.getParameter("task").equals("logincheck")){
                    String username = request.getSession().getAttribute("username").toString();
                    JSONObject jso = new JSONObject();
                    if(username != null && username.length() > 0){
                        response.setStatus(200);
                        jso.put("result", "Sikeres bejelentkezés!");
                        jso.put("username", username);
                    }
                    else{
                        jso.put("result", "Nincs joga a lap megtekintéséhez!");
                        response.setStatus(403);
                    }
                    out.write(jso.toString());
            }
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
