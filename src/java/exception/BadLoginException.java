package exception;

import service.XmlExceptionService;

public class BadLoginException extends Exception{

    private XmlExceptionService service;
    
    public BadLoginException(String message) {
        super(message);
        this.service = new XmlExceptionService();
        
        this.service.writeToXml(this);
    }
    
    
    public void showMessage(){
        System.out.println("Exception: " + this.getMessage());
    }
    
    public void showMessageByErrorCode(Integer errorCode){
        if(errorCode.equals(404)) System.out.println("A lap nem található");
        if(errorCode.equals(500)) System.out.println("Internal server error");
        if(errorCode.equals(502)) System.out.println("Nem emlékszem hiba");
        
    }
    
}
