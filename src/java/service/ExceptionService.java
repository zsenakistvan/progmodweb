/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package service;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import model.ExceptionModel;

/**
 *
 * @author zsenakistvan
 */
public class ExceptionService {
    
    public List<ExceptionModel> getAllException(){
        List<ExceptionModel> allException = new ArrayList<>();
        try{
            allException = ExceptionModel.getAllException();
            /*
            Ha nem lenne statikus a getAllException metódus, akkor:
            ExceptionModel em = new ExceptionModel();
            allException = em.getAllException()
            */
        }
        catch(Exception ex){
            System.out.println(ex.toString());
        }
        return allException;
    } 
    
}
