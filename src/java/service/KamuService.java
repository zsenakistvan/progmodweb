
package service;

import model.Kamu;
import exception.BadLoginException;

public class KamuService {
    
    public Boolean KamuNullPointer() throws BadLoginException{
        Kamu k = new Kamu(1);
        k.setKamuSzam(null);
        try{
            Integer a = k.getKamuSzam();
            a++;
            return Boolean.FALSE;
        }
        catch(Exception ex){
            throw new BadLoginException("Próba bad login");
            //XmlExceptionService xes = new XmlExceptionService();
            //xes.writeToXml(ex);
        }
        //return Boolean.TRUE;
    }
    
    public Boolean KamuOutOfBound() throws BadLoginException{
        Kamu k = new Kamu(1);
        
        try{
            String a = k.getKamuTomb().get(3);
            
            return Boolean.FALSE;
        }
        catch(Exception ex){
            throw new BadLoginException("Ez meg a out of bound próbája");
            //XmlExceptionService xes = new XmlExceptionService();
            //xes.writeToXml(ex);
        }
        //return Boolean.TRUE;
    }
    
}
