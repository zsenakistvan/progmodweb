package service;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import model.XmlException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import java.util.UUID;
import annotation.CustomColor;

public class XmlExceptionService {

    
    private static File logFile = new File("../../../../../../Documents/GitHub/progmodweb/XmlExceptionLog.xml");
    public Integer counter;// A generikus teszthez lett felvéve, amúgy értelmetlen
    
    public List<XmlException> xmlExceptionLogs(){
        List<XmlException> exceptions = new ArrayList<>();
        try{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xml = builder.parse(logFile);
            xml.normalize();
            NodeList exceptionNodes = xml.getElementsByTagName("XmlExceptionLog");
            for(Integer i = 0; i < exceptionNodes.getLength(); i++){
                Element exceptionElement = (Element)exceptionNodes.item(i);
                String message = exceptionElement.getElementsByTagName("message").item(0).getTextContent();
                String stackTrace = exceptionElement.getElementsByTagName("stackTrace").item(0).getTextContent();
                String source = exceptionElement.getElementsByTagName("source").item(0).getTextContent();
                String type = exceptionElement.getElementsByTagName("type").item(0).getTextContent();
                String date = exceptionElement.getElementsByTagName("date").item(0).getTextContent();
                String guid = exceptionElement.getAttribute("guid");
                XmlException exception = new XmlException(date, message, stackTrace, source, type, guid);
                exceptions.add(exception);
            }
        }
        catch(Exception ex){
            System.out.println(ex.toString());
            System.out.println(Arrays.toString(ex.getStackTrace()));
        }
        return exceptions;
    }
    
    public JSONArray toJson(List<XmlException> exceptions) throws NoSuchFieldException{
        JSONArray jsons = new JSONArray();
        for(XmlException ex : exceptions){
            JSONObject json = new JSONObject();
            json.put("message", ex.getMessage());
            try{
            json.put("customColor", ex.getClass().getField("message").getAnnotation(CustomColor.class).value());
            }
            catch(Exception e){
                System.out.println(e.toString());
            }
            json.put("type", ex.getType());
            json.put("date", ex.getDate());
            json.put("guid", ex.getGuid());
            jsons.put(json);
        }
        
        return jsons;
    }

    public XmlException getExData(String guidFromClient) {
        XmlException ex = null;
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        try{
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document xml = builder.parse(logFile);
            xml.normalize();
            NodeList exceptionNodes = xml.getElementsByTagName("XmlExceptionLog");
            for(Integer i = 0; i < exceptionNodes.getLength(); i++){
                Element e = (Element)exceptionNodes.item(i);
                String guid = e.getAttribute("guid");
                if(guid.equals(guidFromClient)){
                    String message = e.getElementsByTagName("message").item(0).getTextContent();
                    String stackTrace = e.getElementsByTagName("stackTrace").item(0).getTextContent();
                    String source = e.getElementsByTagName("source").item(0).getTextContent();
                    String type = e.getElementsByTagName("type").item(0).getTextContent();
                    String date = e.getElementsByTagName("date").item(0).getTextContent();
                    ex = new XmlException(date, message, stackTrace, source, type, guid);
                
                }
            }
        }
        catch(Exception e){
            System.out.println(e.toString());
        }
        
        
        return ex;
    }

    public JSONObject exDataToJson(XmlException ex) {
            JSONObject json = new JSONObject();
            json.put("message", ex.getMessage());
            json.put("customColor", ex.getClass().getAnnotation(CustomColor.class));
            json.put("type", ex.getType());
            json.put("date", ex.getDate());
            json.put("guid", ex.getGuid());
            json.put("stackTrace", ex.getStackTrace());
            json.put("source", ex.getSource());
            return json;
    
    }
    
    public Boolean writeToXml(Exception ex){
        try{
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            File f = logFile;
            Document xml = builder.parse(f);
            xml.normalize();
            Element root = (Element)xml.getFirstChild();
            Element xmlExceptionLog = xml.createElement("XmlExceptionLog");
            Element date = xml.createElement("date");
            Element message = xml.createElement("message");
            Element stack = xml.createElement("stackTrace");
            Element source = xml.createElement("source");
            Element type = xml.createElement("type");
            date.setTextContent(new Date().toString());
            message.setTextContent(ex.toString());
            stack.setTextContent(Arrays.toString(ex.getStackTrace()));
            source.setTextContent(ex.getClass().getSimpleName());//FIXME: Átvezetni a megfelelő exception osztályba
            type.setTextContent(ex.getMessage());
            xmlExceptionLog.appendChild(date);
            xmlExceptionLog.appendChild(message);
            xmlExceptionLog.appendChild(stack);
            xmlExceptionLog.appendChild(source);
            xmlExceptionLog.appendChild(type);
            xmlExceptionLog.setAttribute("guid", UUID.randomUUID().toString());
            root.appendChild(xmlExceptionLog);
            
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer();
            DOMSource domSource = new DOMSource(xml);
            StreamResult result = new StreamResult(f);
            transformer.transform(domSource, result);
            return Boolean.TRUE;
            
        }
        catch(Exception e){
            System.out.println(e.toString());
        }
        return Boolean.FALSE;
    }
 


}
