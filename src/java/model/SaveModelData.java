/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.File;
import java.util.Arrays;
import java.util.Date;
import java.util.UUID;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author zsenakistvan
 */
public class SaveModelData <T>{
    
    public void save(T entity, String fieldName){
        try{
        String data = entity.getClass().getField(fieldName).toString();
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            DocumentBuilder builder = factory.newDocumentBuilder();
            File f =new File( "../../../../../../Documents/GitHub/progmodweb/ModelData.xml");
            Document xml = builder.parse(f);
            xml.normalize();
            Element root = (Element)xml.getFirstChild();
            Element dataToSave = xml.createElement("data");
            dataToSave.setTextContent(data);
            root.appendChild(dataToSave);
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transformer = tFactory.newTransformer();
            DOMSource domSource = new DOMSource(xml);
            StreamResult result = new StreamResult(f);
            transformer.transform(domSource, result);
        }
        catch(Exception ex){
            System.out.println(ex.toString());
        }
    }
    
}
