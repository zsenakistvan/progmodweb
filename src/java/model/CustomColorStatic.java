/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author zsenakistvan
 */
public abstract class CustomColorStatic {
    
    public static final String RED = "red";
    public static final String GREEN = "#00FF00";
    public static final String LIGHTGREEN = "#70FF70";
    public static final String TANSPARENTRED = "rgba(255, 0, 0, 0.7)";
    
}
