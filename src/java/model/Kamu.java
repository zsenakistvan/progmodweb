package model;

import java.util.List;
import java.util.ArrayList;

public class Kamu {
    
    public Integer KamuSzam;
    public List<String> KamuTomb = new ArrayList<>();

    public Kamu(Integer KamuSzam) {
        this.KamuSzam = KamuSzam;
        this.KamuTomb.add("semmi");
    }

    public Integer getKamuSzam() {
        return KamuSzam;
    }

    public void setKamuSzam(Integer KamuSzam) {
        this.KamuSzam = KamuSzam;
    }

    public List<String> getKamuTomb() {
        return KamuTomb;
    }

    public void setKamuTomb(List<String> KamuTomb) {
        this.KamuTomb = KamuTomb;
    }
    
    
    
    
    
}
