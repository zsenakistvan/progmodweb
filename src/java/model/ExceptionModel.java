/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.ParameterMode;
import javax.persistence.Persistence;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author zsenakistvan
 */
@Entity
@Table(name = "exception")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Exception.findAll", query = "SELECT e FROM Exception e")
    , @NamedQuery(name = "Exception.findById", query = "SELECT e FROM Exception e WHERE e.id = :id")
    , @NamedQuery(name = "Exception.findByDate", query = "SELECT e FROM Exception e WHERE e.date = :date")
    , @NamedQuery(name = "Exception.findByMessage", query = "SELECT e FROM Exception e WHERE e.message = :message")
    , @NamedQuery(name = "Exception.findBySource", query = "SELECT e FROM Exception e WHERE e.source = :source")
    , @NamedQuery(name = "Exception.findByType", query = "SELECT e FROM Exception e WHERE e.type = :type")
    , @NamedQuery(name = "Exception.findByGuid", query = "SELECT e FROM Exception e WHERE e.guid = :guid")
    , @NamedQuery(name = "Exception.findByIsActive", query = "SELECT e FROM Exception e WHERE e.isActive = :isActive")})
public class ExceptionModel implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    @Basic(optional = false)
    @NotNull
    @Column(name = "date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "message")
    private String message;
    @Basic(optional = false)
    @NotNull
    @Lob
    @Size(min = 1, max = 65535)
    @Column(name = "stacktrace")
    private String stacktrace;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "source")
    private String source;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 1000)
    @Column(name = "type")
    private String type;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 100)
    @Column(name = "guid")
    private String guid;
    @Basic(optional = false)
    @NotNull
    @Column(name = "is_active")
    private int isActive;

    public ExceptionModel() {
    }

    public ExceptionModel(Integer id) {
        this.id = id;
    }

    public ExceptionModel(Integer id, Date date, String message, String stacktrace, String source, String type, String guid, int isActive) {
        this.id = id;
        this.date = date;
        this.message = message;
        this.stacktrace = stacktrace;
        this.source = source;
        this.type = type;
        this.guid = guid;
        this.isActive = isActive;
    }

    public Integer getId() {
        return id;
    }

    public Date getDate() {
        return date;
    }


    public String getMessage() {
        return message;
    }

    public String getStacktrace() {
        return stacktrace;
    }


    public String getSource() {
        return source;
    }


    public String getType() {
        return type;
    }

    public String getGuid() {
        return guid;
    }


    public int getIsActive() {
        return isActive;
    }
    
    
    public static ExceptionModel getExceptionByGuid(String guid){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProgmodWebPU");
        EntityManager manager = factory.createEntityManager();
        StoredProcedureQuery spq = manager.createStoredProcedureQuery("get_exception_by_guid");
        spq.registerStoredProcedureParameter("id_out", Integer.class, ParameterMode.OUT);
        spq.registerStoredProcedureParameter("guid_in", String.class, ParameterMode.IN);
        spq.setParameter("guid_in", guid);
        spq.execute();
        Integer id = (Integer)spq.getOutputParameterValue("id_out");//if id != null...
        ExceptionModel em = manager.find(ExceptionModel.class, id);
        return em;
    }
    
    
    public void setToInactive(){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProgmodWebPU");
        EntityManager manager = factory.createEntityManager();
        StoredProcedureQuery spq = manager.createStoredProcedureQuery("set_exception_to_inactive");
        spq.registerStoredProcedureParameter("id_in", Integer.class, ParameterMode.IN);
        spq.setParameter("id_in", this.id);
        spq.execute();
    }
    
    public void setToActive(){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProgmodWebPU");
        EntityManager manager = factory.createEntityManager();
        ExceptionModel em = manager.find(ExceptionModel.class, this.id);
        manager.getTransaction().begin();
        em.isActive = 1;
        manager.getTransaction().commit();
    }
    
    
    public static ExceptionModel readException(Integer id){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProgmodWebPU");
        EntityManager manager = factory.createEntityManager();
        ExceptionModel em = manager.find(ExceptionModel.class, id);
        return em;
    }
    
    public static ExceptionModel readException_old(Integer id){
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProgmodWebPU");
        EntityManager manager = factory.createEntityManager();
        StoredProcedureQuery spq = manager.createStoredProcedureQuery("read_exception");
        spq.registerStoredProcedureParameter("id_in", Integer.class, ParameterMode.IN);
        spq.setParameter("id_in", id);
        ExceptionModel em = (ExceptionModel)spq.getSingleResult();
        return em;
    }

    public static List<ExceptionModel> getAllException(){
        List<ExceptionModel> allException = new ArrayList<>();
        try{
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("ProgmodWebPU");
        EntityManager manager = factory.createEntityManager();
        StoredProcedureQuery spq = manager.createStoredProcedureQuery("get_all_exception");
        allException = spq.getResultList();
        }
        catch(Exception ex){
            System.out.println("hiba: " + ex.toString());
            
        }
        return allException;
    }
    
    
    
    
    
    
    
    

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Exception)) {
            return false;
        }
        ExceptionModel other = (ExceptionModel) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "model.Exception[ id=" + id + " ]";
    }
    
}
