package model;

import annotation.CustomColor;



public class XmlException {
    
    private String date;
    @CustomColor(value = CustomColorStatic.TANSPARENTRED)//Enum helyett
    public String message;
    private String stackTrace;
    private String source;
    private String type;
    private String guid;

    public XmlException(String date, String message, String stackTrace, String source, String type, String guid) {
        this.date = date;
        this.message = message;
        this.stackTrace = stackTrace;
        this.source = source;
        this.type = type;
        this.guid = guid;
    }

    public String getDate() {
        return date;
    }

    public String getMessage() {
        return message;
    }

    public String getStackTrace() {
        return stackTrace;
    }

    public String getSource() {
        return source;
    }

    public String getType() {
        return type;
    }
    
    public String getGuid(){
        return this.guid;
    }
    
    
    
}
